#!/usr/bin/bash

echo "${COLOR_WHITE} "
echo "${COLOR_ORANGE} copy my wallpaper"
echo "${COLOR_WHITE} "

GIT_DIRECTORY="/home/$USER/git/my-wallpaper"
if [ -d $GIT_DIRECTORY ];then rm -rf $GIT_DIRECTORY ; fi
mkdir -p $GIT_DIRECTORY

git clone https://git@gitlab.com/8u3631984/my-wallpaper.git  $GIT_DIRECTORY

cd $INSTALL_DIR