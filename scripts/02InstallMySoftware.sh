#!/usr/bin/bash

echo "${COLOR_WHITE} "
echo "${COLOR_ORANGE} install my software"
echo "${COLOR_WHITE} "

if pacman -Qs paru > /dev/null ; then
  echo "paru is already installed"
else
  mkdir -p /tmp/paru
  git clone https://aur.archlinux.org/paru.git /tmp/paru

  cd /tmp/paru &&  makepkg -si --noconfirm
  cd $INSTALL_DIR
  rm -rf /tmp/paru
fi

cd $INSTALL_DIR/scripts
paru -S --needed --noconfirm - < packageList.txt
cd $INSTALL_DIR