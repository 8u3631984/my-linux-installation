#!/usr/bin/bash

echo "${COLOR_WHITE} "
echo "${COLOR_ORANGE} copy my dotfiles"
echo "${COLOR_WHITE} "

GIT_DIRECTORY="/home/$USER/git/my-dotfiles"
if [ -d $GIT_DIRECTORY ];then rm -rf $GIT_DIRECTORY ; fi
mkdir -p $GIT_DIRECTORY

git clone https://git@gitlab.com/8u3631984/my-dotfiles.git  $GIT_DIRECTORY

CONFIG_DIRECTORY="/home/$USER/.config"
if [ ! -d $CONFIG_DIRECTORY ];then mkdir -p $CONFIG_DIRECTORY ; fi

cd $GIT_DIRECTORY/config && stow . --adopt  --target $CONFIG_DIRECTORY
cd $INSTALL_DIR