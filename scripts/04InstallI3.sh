#!/usr/bin/bash

echo "${COLOR_WHITE} "
echo "${COLOR_ORANGE} install window manager"
echo "${COLOR_WHITE} "

paru -S --needed --noconfirm autotiling i3-wm dmenu polybar alacritty picom feh
cd $INSTALL_DIR