#!/usr/bin/bash

echo "${COLOR_WHITE} "
echo "${COLOR_ORANGE} install service"
echo "${COLOR_WHITE} "

# cups service
sudo pacman -S --needed --noconfirm cups cups-pdf
sudo systemctl enable cups.service

# docker service
sudo pacman -S --needed --noconfirm docker docker-compose
sudo systemctl enable docker.service

# sshd service
sudo pacman -S --needed --noconfirm openssh
sudo systemctl enable sshd.service

# openssh service
sudo pacman -S --needed --noconfirm ntp
sudo systemctl enable ntpd.service
sudo ntpdate pool.ntp.org

# lightdm service
sudo pacman -S --needed --noconfirm lightdm lightdm-gtk-greeter
sudo systemctl enable lightdm.service

cd $INSTALL_DIR