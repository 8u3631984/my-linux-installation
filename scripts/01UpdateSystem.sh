#!/usr/bin/bash

echo "${COLOR_WHITE} "
echo "${COLOR_ORANGE} update system"
echo "${COLOR_WHITE} "

sudo cp scripts/configuration/pacman/pacman.conf /etc/pacman.conf

sudo pacman -Syyu --needed --noconfirm
cd $INSTALL_DIR