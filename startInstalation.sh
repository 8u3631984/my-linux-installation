#!/usr/bin/bash

# define globals variables

export INSTALL_DIR=$(pwd)

export COLOR_WHITE=$'\033[0;37m'
export COLOR_ORANGE=$'\033[0;33m'
export COLOR_RED=$'\033[0;31m'

echo "${COLOR_ORANGE} $(< scripts/banner.txt)"
echo "${COLOR_WHITE} "

source scripts/01UpdateSystem.sh
source scripts/02InstallMySoftware.sh
source scripts/03InstallServices.sh
source scripts/04InstallI3.sh
source scripts/05CopyMyDotfiles.sh
source scripts/06CopyMyWallpaper.sh