# My Linux Installation

## Linux Arch

### Vorbereitung
- [iso herunterladen](https://www.archlinux.de/download)
- das ISO auf CD oder DVD brennen, oder auf ein (ausgehängtes) USB-Medium kopieren

### Installation starten
- nach dem Start des Images die Installation mit ```archinstall``` starten

### Sprache für Archinstall
- auf deutsch unmstellen

### Spiegelserver
- auf regional (Deutschland) umstellen

### Lokalisierung
- Tasturlayout : de
- Lokale Sprache : de_DE.UTF-8
- Lokale kodierung : UTF-8

### Laufwerkskonfiguration
- empfohlene Konfiguration verwenden

### Bootloader
- grub

### SWAP
- auf true setzen

### Profil
- auf minimal setzen

---